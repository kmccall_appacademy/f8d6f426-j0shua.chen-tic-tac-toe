require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :current_player

  def initialize(player1, player2)
    @board = Board.new
    @player1 = player1
    @player2 = player2
    @current_player = player1
    @player1.mark = :X
    @player2.mark = :O
  end

  def play_turn
    move = @current_player.get_move
    @board.place_mark(move, @current_player.mark)
    self.switch_players!
  end

  def switch_players!
    if @current_player == @player1
      @current_player = @player2
    else
      @current_player = @player1
    end
  end

end
