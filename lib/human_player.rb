class HumanPlayer
  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def get_move
    print "Please enter a move: "
    move = gets.chomp
    until valid?(move)
      print "Please enter a valid move: "
      move = gets.chomp
    end
    self.to_int(move)

  end

  def valid?(move)
    move = self.to_int(move)
    return false if move.length != 2
    move.all? {|n| n.between?(0,2)}
  end

  def to_int(move)
    move.split(",").map(&:to_i)
  end

  def display(b)
    puts
    puts b.printer
  end

end
