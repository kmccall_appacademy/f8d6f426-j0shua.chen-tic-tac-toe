class ComputerPlayer
  attr_accessor :name, :mark, :board

  def initialize(name)
    @name = name
    @mark = mark
  end

  def display(board)
    @board = board
  end

  def get_move
    @board.empty_pos.each {|pos| return pos if @board.winning_move(pos, @mark)}
    @board.empty_pos.sample
  end

end
