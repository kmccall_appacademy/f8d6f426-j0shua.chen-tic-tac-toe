class Board
  attr_accessor :grid

  def initialize(grid = Array.new(3) {Array.new(3)})
    @grid = grid
  end

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def []=(pos, val)
    x, y = pos
    @grid[x][y] = val
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    [:X, :O].each {|mark| return mark if self.won?(mark)}
    nil
  end

  def won?(mark)
    lines.any? do |line|
      line.all? {|spot| self[spot] == mark}
    end
  end

  def tie
    @grid.flatten.all? {|spot| spot != nil}
  end

  def over?
    self.winner || self.tie
  end

  def winning_move(pos, mark)
    self.place_mark(pos, mark)
    ftw = winner == mark
    self[pos]= nil
    ftw
  end

  def all_pos
    spot =[]
    3.times do |row|
      3.times {|col| spot << [row, col]}
    end
    spot
  end

  def empty_pos
    self.all_pos.select {|spots| self.empty?(spots)}
  end

  def printer
    @grid.map do |row|
      row.map {|spot| spot.nil? ? "_" : spot}.join(" ")
    end.join("\n")
  end

  def lines

    h = (0..2).map do |x|
      [[x, 0], [x, 1], [x, 2]]
    end

    v = (0..2).map do |y|
      [[0, y], [1, y], [2, y]]
    end

    d = [

      [[0, 0], [1, 1], [2, 2]],
      [[0, 2], [1, 1], [2, 0]]

    ]

    h+v+d
  end

end
